import $ from 'jquery';

window.$ = window.jQuery = $;
$('.userName').hover(function () {
  $('.nameHover').show()
  $('.userName li').css('width', $('.userName').width() - 20)
}, function () {
  $('.nameHover').hide()
})
$('.productCenter').hover(function () {
  $('.upload').addClass('centerColor')
  $('.productHover').show()
}, function () {
  $('.upload').removeClass('centerColor')
  $('.productHover').hide()
})

var _hmt = _hmt || [];
(function () {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?145c730693250942f5bac10e97080c2f";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(hm, s);
})();

function banner() {
  var bn_id = 0;
  var bn_id2 = 1;
  var speed33 = 3000;
  var qhjg = 1;
  var MyMar33;
  $("#banner .d1").eq(0).fadeIn();
  if ($("#banner .d1").length > 1) {
    $("#banner_id li").eq(0).addClass("nuw");

    function Marquee33() {
      bn_id2 = bn_id + 1;
      if (bn_id2 > $("#banner .d1").length - 1) {
        bn_id2 = 0;
      }
      $("#banner .d1").eq(bn_id).css("z-index", "2");
      $("#banner .d1").eq(bn_id2).css("z-index", "1");
      $("#banner .d1").eq(bn_id).fadeOut('fast', function () {
        $("#banner .d1").eq(bn_id2).show();
      });
      $("#banner_id li").removeClass("nuw");
      $("#banner_id li").eq(bn_id2).addClass("nuw");
      bn_id = bn_id2;
    };

    MyMar33 = setInterval(Marquee33, speed33);

    $("#banner_id li").click(function () {
      var bn_id3 = $("#banner_id li").index(this);
      if (bn_id3 != bn_id && qhjg == 1) {
        qhjg = 0;
        $("#banner .d1").eq(bn_id3).show();

        $("#banner .d1").eq(bn_id).fadeOut(function () {
          qhjg = 1;
          $("#banner .d1").eq(bn_id).css("z-index", "2");
          $("#banner .d1").eq(bn_id3).css("z-index", "1");
          $("#banner_id li").removeClass("nuw");
          $("#banner_id li").eq(bn_id3).addClass("nuw");
          bn_id = bn_id3;
        });

      }
    })
    $("#banner_id,.d1").hover(function () {
      clearInterval(MyMar33);
    }, function () {
      MyMar33 = setInterval(Marquee33, speed33);
    })
  } else {
    $("#banner_id").hide();
  }
}

$(function () {
  banner();
})

jQuery(".picScroll_left").slide({
  mainCell: ".bd ul", scroll: 1, effect: "left", autoPlay: true, vis: 4, autoPage: true, pnLoop: true, interTime: 6000
});

$(function () {
  startRotaryTable({
    "linkageFlag": true, "animateTo": 24 * 60 * 60 * 366 * 12, "duration": 24 * 60 * 60 * 2000, "angle": 0
  }); //开始联动


  $(".index_roate_list > ul > li").on("mouseover", function () { //安全服务体系鼠标事件
    var parentsLi = $(this).parents("li");
    $(".index_roate_list > ul > li").removeClass("on");
    $(this).addClass("on");
    $(".index_roate_item").stopRotate(); //停止转动
    if ($(this).hasClass("content1")) {
      startRotaryTable({
        "linkageFlag": false, "duration": 4800, "angle": 120
      }); //开始联动
    } else if ($(this).hasClass("content2")) {
      startRotaryTable({
        "linkageFlag": false, "duration": 4800, "angle": 260
      }); //开始联动
    } else if ($(this).hasClass("content3")) {
      startRotaryTable({
        "linkageFlag": false, "duration": 4800, "angle": 20
      }); //开始联动
    }
  });

  $(".index_roate_list > ul").on("mouseleave", function () { //鼠标离开事件
    startRotaryTable({
      "linkageFlag": true, "animateTo": 24 * 60 * 60 * 366 * 12, "duration": 24 * 60 * 60 * 2000, "angle": 0
    }); //开始联动
  });
  setInterval(function () {
    GotoBtns();
  }, 1500);
  $(".leftBottomDialog").addClass("leftBottomUp");
  $(".leftBottom_closed").on("click", function () {
    $(".leftBottomDialog").removeClass("leftBottomUp");
  });
  $("body").on("mouseover", ".index_brand_wrap > li", function () { //品牌资讯鼠标事件
    var prev = $(this).prev("i");
    var next = $(this).next("i");
    $(prev).css({
      backgroundColor: "#e5e5e5", "opacity": 0
    });
    $(next).css({
      backgroundColor: "#e5e5e5", "opacity": 0
    })
  }).on("mouseleave", ".index_brand_wrap > li", function () {
    var prev = $(this).prev("i");
    var next = $(this).next("i");
    $(prev).css({
      backgroundColor: "#e5e5e5", "opacity": 1
    });
    $(next).css({
      backgroundColor: "#e5e5e5", "opacity": 1
    })
  });
});

/**
 * 功能： 是否开始转盘
 * @param  linkageFlag :联动标志， true - 联动  false-不联动
 * @param  animateTo : 转动目标角度
 * @param  duration : 旋转持续时间，以毫秒为单位
 **/
function GotoBtns() { //GotoBtns 一闪一闪
  $(".GotoBtn").animate({
    "opacity": "0.8"
  }, 500, function () {
    $(".GotoBtn").animate({
      "opacity": "1.0"
    }, 500)
  })
}

function startRotaryTable(optset) {
  $(".index_roate_item").rotate({
    angle: optset.angle,
    animateTo: optset.animateTo,
    easing: $.easing.easeInOutExpo,
    duration: optset.duration,
    callback: function () {
    },
    step: function (currentRange) {
      if (optset.linkageFlag == true) {
        /* var angle = $(".index_roate_item").getRotateAngle();   //转盘转了多少d */
        var angle1 = currentRange % 360;
        $(".index_roate_list > ul > li").removeClass("on");
        if (angle1 < 80 && angle1 >= 0) {
          $(".index_roate_list > ul > li.content3").addClass("on");
        } else if (angle1 < 200 && angle1 >= 80) {
          $(".index_roate_list > ul > li.content1").addClass("on");
        } else if (angle1 < 360 && angle1 >= 200) {
          $(".index_roate_list > ul > li.content2").addClass("on");
        }
      }
    }
  });
}

//
// function times(obj, tops, types) {
//   var m;
//   if (types == "dots") {
//     m = Math.floor(Math.random() * 10 + 11);
//   } else {
//     m = Math.floor(Math.random() * 40 + 15);
//   }
//   obj.stop(true, true).animate({
//     "top": tops + m + "px"
//   }, 700, function () {
//     if (types == "dots") {
//       obj.stop(true, true).animate({
//         "top": tops + "px"
//       }, 700);
//     } else if (types == "nums") {
//       obj.stop(true, true).animate({
//         "top": 0 + "px"
//       }, 700);
//     }
//
//   })
// }

$(function () {
  var wqs = $("#index_safe").offset().top - 64;
  var srls = $(document).scrollTop();
  if (srls >= wqs) {
    $(".header_box").addClass("scrollHeader")
  } else {
    $(".header_box").removeClass("scrollHeader");
  }
  headerScroll();

  $(".bannerLogo").addClass("loadBannerLogo");
  $(window).scroll(function () {
    headerScroll();
  })
});

function headerScroll() {
  var wq = $("#index_safe").offset().top - 164;
  var srl = $(document).scrollTop();
  if (srl < 66) {
    $(".index_header_box").removeClass("miniHeader");
  } else if (srl >= 66 && srl < wq) {
    $(".index_header_box").addClass("miniHeader").removeClass("scrollHeader");
  } else if (srl >= wq) {
    $(".index_header_box").addClass("scrollHeader").removeClass("miniHeader");
  } else {
    $(".index_header_box").removeClass("scrollHeader");
  }
}

function getXAndY(event) {
  // Ev = event || window.event;
  var mousePos = mouseCoords(event);
  var x = mousePos.x;
  var y = mousePos.y;
  var x1 = $(".bannerImage").offset().left;
  var y1 = $(".bannerImage").offset().top;
  var x2 = x - x1;
  var y2 = y - y1;
  return {
    x: x2, y: y2
  };
}

function mouseCoords(event) {
  if (event.pageX || event.pageY) {
    return {
      x: event.pageX, y: event.pageY
    };
  }
  return {
    x: event.clientX + document.body.scrollLeft - document.body.clientLeft,
    y: event.clientY + document.body.scrollTop - document.body.clientTop
  };
}

function times(obj, tops, types, speed) {
  var m;
  if (types == "dots") {
    m = Math.floor(Math.random() * 10 + 21);
  } else {
    m = Math.floor(Math.random() * 50 + 31);
  }
  obj.stop(true, true).animate({
    "top": tops + m + "px"
  }, speed, function () {
    if (types == "dots") {
      obj.stop(true, true).animate({
        "top": tops + "px"
      }, speed);
    } else if (types == "nums") {
      obj.stop(true, true).animate({
        "top": 0 + "px"
      }, speed);
    }

  })
}

$("img.lazy").lazyload({
  threshold: 200
});

var SEPARATION = 100, AMOUNTX = 50, AMOUNTY = 50;

var container;
var camera, scene, renderer;

var particles, particle, count = 0;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = 100 / 2;

init();
animate();

function init() {

  container = document.createElement('div');
  var bg_wave01 = document.getElementById("bg_wave01");
  bg_wave01.appendChild(container);

  camera = new THREE.PerspectiveCamera(75, window.innerWidth / 100, 1, 10000);
  camera.position.z = 1000;

  scene = new THREE.Scene();

  particles = new Array();

  var PI2 = Math.PI * 2;
  var material = new THREE.ParticleCanvasMaterial({

    color: 0xffffff, program: function (context) {

      context.beginPath();
      context.arc(0, 0, 1, 0, PI2, true);
      context.fill();

    }

  });

  var i = 0;

  for (var ix = 0; ix < AMOUNTX; ix++) {

    for (var iy = 0; iy < AMOUNTY; iy++) {

      particle = particles[i++] = new THREE.Particle(material);
      particle.position.x = ix * SEPARATION - ((AMOUNTX * SEPARATION) / 2);
      particle.position.z = iy * SEPARATION - ((AMOUNTY * SEPARATION) / 2);
      scene.add(particle);

    }

  }
  renderer = new THREE.CanvasRenderer();
  renderer.setSize(window.innerWidth, 100);
  container.appendChild(renderer.domElement);
}

function animate() {
  requestAnimationFrame(animate);
  render();
}

function render() {

  camera.position.x += (mouseX - camera.position.x) * .05;
  camera.position.y += (-mouseY - camera.position.y) * .05;
  camera.lookAt(scene.position);
  var i = 0;
  for (var ix = 0; ix < AMOUNTX; ix++) {
    for (var iy = 0; iy < AMOUNTY; iy++) {

      particle = particles[i++];
      particle.position.y = (Math.sin((ix + count) * 0.3) * 50) + (Math.sin((iy + count) * 0.5) * 50);
      particle.scale.x = particle.scale.y = (Math.sin((ix + count) * 0.3) + 1) * 2 + (Math.sin((iy + count) * 0.5) + 1) * 2;

    }
  }
  renderer.render(scene, camera);
  count += 0.1;
}

$(function () {
  function v(a, c) {
    var v, h = this;
    this.name = $("." + a), this.img = this.name.find("img"), this.index = 0, this.start = function () {
      v = setInterval(function () {
        h.index < h.img.length ? h.index++ : h.index = 0, h.img.hide().eq(h.index - 1).show()
      }, c)
    }, this.End = function () {
      clearTimeout(v), v = null
    }
  }

  var g = new v("creat-title", 400);
  g.start();
})
