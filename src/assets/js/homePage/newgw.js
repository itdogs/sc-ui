/**
 * Created by Administrator on 2017/10/27 0027.
 */
$(function(){
    //nav滚动定位
    var p=0;
    var t=0;
    $(".mouse_move").on("click",function(){
        $('html,body').animate({
            scrollTop: jQuery("#index_safe").offset().top-64
        },500);
    });
    $(".navs_box_left li a").on("mouseover",function(){
        var forId=$(this).parent("li").attr("data-for");
        if(!$(this).parent("li").hasClass("active")){
            $(this).parent("li").addClass("active").siblings("li").removeClass("active");
            $("#"+forId).show().siblings("div").hide();
        }
    });
    $.each($(".index_solve_list"),function(){
        var listLength= $(this).find("a").length;
        $("#index_solve").on("mouseenter",function(){
            if(listLength>4){
                $(this).siblings(".index_solve_net").show();
            }else{
                $(this).siblings(".index_solve_net").hide();
            }
        }).on("mouseleave",function(){
            $(this).siblings(".index_solve_net").hide();
        })
    });
    var flasg1=true;
    $("span.index_solve_net").on("click",function(){
        if(flasg1==true){
            flasg1=false;
            var leftOld=$(this).siblings(".index_solve_list").children("div").position().left;
            if(!$(this).hasClass("disabled")){
                leftOld-=288;
                $(this).siblings(".index_solve_list").children("div").css({
                    "-webkit-transition":"-webkit-transform 500ms ease-out",
                    "transform":"translate("+leftOld+"px,0) scale(1) translateZ(0px)"
                });
                $(this).siblings(".index_solve_prev").removeClass("disabled").show();
                var listL=$(this).siblings(".index_solve_list").find("a").length;
                var Max=(listL-4)*288;
                if(-leftOld>=Max){
                    $(this).addClass("disabled").hide();
                }
            }
            setTimeout(function(){
                flasg1=true;
                console.log(flasg1);
            },500)
        }

    });
    var uflasg1=true;
    $("span.index_solve_prev").on("click",function(){
        if(uflasg1==true){
            uflasg1=false;
            var leftOld=$(this).siblings(".index_solve_list").children("div").position().left;
            if(!$(this).hasClass("disabled")){
                leftOld+=288;
                $(this).siblings(".index_solve_list").children("div").css({
                    "-webkit-transition":"-webkit-transform 500ms ease-out",
                    "transition":"transform 500ms ease-out",
                    "transform":"translate("+leftOld+"px,0) scale(1) translateZ(0px)"
                });
                $(this).siblings(".index_solve_net").removeClass("disabled").show();
                if(leftOld>-288){
                    $(this).addClass("disabled").hide();
                }
            }
            setTimeout(function(){
                uflasg1=true;
            },500)
        }
    });


    $(".index_solve_net").on("mouseenter",function(){
        $(this).addClass("hoverMspan");
    }).on("mouseleave",function(){
        $(this).removeClass("hoverMspan");
    })
    $(".index_solve_prev").on("mouseenter",function(){
        $(this).addClass("hoverMspan");
    }).on("mouseleave",function(){
        $(this).removeClass("hoverMspan");
    })



    var _index_mid=$(".index_mid1_nav>ul li");
    var arrImages=[
        "view/images/index/bgicon1.jpg",
        "view/images/index/bgicon2.jpg",
        "view/images/index/bgicon3.jpg",
        "view/images/index/bgicon4.jpg",
        "view/images/index/bgicon5.jpg",
        "view/images/index/bgicon6.jpg",
        "view/images/index/bgicon7.jpg",
        "view/images/index/bgicon8.jpg"];
    preloadimages(arrImages).done(function(images){
        $('.index_mid1_mask1').css({
            "background":'url('+images[0].src+') no-repeat 50% center'
        });
        $('.index_mid1_mask2').css({
            "background":'url('+images[1].src+') no-repeat center center'
        });
        $('.index_mid1_mask3').css({
            "background":'url('+images[2].src+') no-repeat center center'
        });
        $('.index_mid1_mask4').css({
            "background":'url('+images[3].src+') no-repeat center center'
        });
        $('.index_mid1_mask5').css({
            "background":'url('+images[4].src+') no-repeat center center'
        });
        $('.index_mid1_mask6').css({
            "background":'url('+images[5].src+') no-repeat center center'
        });
        $('.index_mid1_mask7').css({
            "background":'url('+images[6].src+') no-repeat center center'
        });
        $('.index_mid1_mask8').css({
            "background":'url('+images[7].src+') no-repeat center center'
        });
        var timesFlag=true;
        $(".index_mid2_low").on("mouseenter",function(){
            timesFlag=false;
        }).on("mouseleave",function(){
            timesFlag=true;
        });
        $(".index_mid1_nav").on("mouseenter",function(){
            timesFlag=false;
        }).on("mouseleave",function(){
            timesFlag=true;
        });
            var timers= setInterval(function(){
                if(timesFlag==true){
                    var activeIndex=  $(".index_mid1_nav").find("li.active").index()>=7?-1:$(".index_mid1_nav").find("li.active").index();
                    $(".index_mid1_nav").find("li").eq(activeIndex+1).click();
                }else{
                     return false;
                }
            },6000);


        _index_mid.on("click",function(){
            if(!$(this).hasClass("active")){
                var _indexOf=$(this).index();
                $(this).addClass("active").siblings("li").removeClass("active");
                $(".index_mid2").find(".index_mid_item").eq(_indexOf).show().addClass("index_mid_up").siblings(".index_mid_item").hide().removeClass("index_mid_up");
            }
        });



        $.each($(".index_mid_ct_list"),function(){
            var listLength= $(this).find("a").length;
            if(listLength>4){
                $(this).siblings(".index_mid_next").show();
            }else{
                $(this).siblings(".index_mid_next").hide();
            }
        });
        var flasg=true;
        $("span.index_mid_next").on("click",function(){
            if(flasg==true){
                flasg=false;
                var leftOld=$(this).siblings(".index_mid_ct_list").children("div").position().left;
                if(!$(this).hasClass("disabled")){
                    leftOld-=283;
                    $(this).siblings(".index_mid_ct_list").children("div").css({
                        "-webkit-transition":"-webkit-transform 500ms ease-out",
                        "transform":"translate("+leftOld+"px,0) scale(1) translateZ(0px)"
                    });
                    $(this).siblings(".index_mid_prev").removeClass("disabled").show();
                    var listL=$(this).siblings(".index_mid_ct_list").find("a").length;
                    var Max=(listL-4)*283;
                    if(-leftOld>=Max){
                        $(this).addClass("disabled").hide();
                    }
                }
                setTimeout(function(){
                    flasg=true;
                },500)
            }

        });
        var uflasg=true;
        $("span.index_mid_prev").on("click",function(){
            if(uflasg==true){
                uflasg=false;
                var leftOld=$(this).siblings(".index_mid_ct_list").children("div").position().left;
                if(!$(this).hasClass("disabled")){
                    leftOld+=283;
                    $(this).siblings(".index_mid_ct_list").children("div").css({
                        "-webkit-transition":"-webkit-transform 500ms ease-out",
                        "transition":"transform 500ms ease-out",
                        "transform":"translate("+leftOld+"px,0) scale(1) translateZ(0px)"
                    });
                    $(this).siblings(".index_mid_next").removeClass("disabled").show();
                    console.log(leftOld);
                    if(leftOld>-283){
                        $(this).addClass("disabled").hide();
                    }
                }
                setTimeout(function(){
                    uflasg=true;
                },500)
            }
        });


        $(".index_mid_next").on("mouseenter",function(){
            if(!$(this).hasClass("disabled")){
                $(this).addClass("hoverMspan");
            }
        }).on("mouseleave",function(){
            $(this).removeClass("hoverMspan");
        })
        $(".index_mid_prev").on("mouseenter",function(){
            if(!$(this).hasClass("disabled")){
                $(this).addClass("hoverMspan");
                return false;
            }
        }).on("mouseleave",function(){
            $(this).removeClass("hoverMspan");
            return false;
        })
    });

    $(".index_brand_tab").find("li").on("click",function(){
        if(!$(this).hasClass("active")){
            var dataWarp="#brand_warp"+$(this).attr("data-warp");

            $(this).addClass("active").siblings("li").removeClass("active");
          $(".index_brand").find(dataWarp).fadeIn(500).siblings("ul").hide();
        }
    });
    var unm=1;
    var newsTimer=setInterval(function(){
        if(unm==1){
            var c=$(".index_brand_tab").find("li.active").index();
            var b;
            if(c==$(".index_brand_tab").find("li").length-1){
                b=0;
            }else{
                b=c+1;
            }
            $(".index_brand_tab").find("li").eq(b).click();
        }

    },4000);
    $("#brand_warp1").on("mouseenter",function(){
        unm=2;
    }).on("mouseleave",function(){
        unm=1;
    })
});
function encStr(str){
	var b = CryptoJS.enc.Hex.parse('31413342354337443945323436383046');
	var c = CryptoJS.enc.Hex.parse('30413242344336443845313335373946');
	var res = CryptoJS.AES.encrypt(str , b, {
		 iv:c,
		 mode: CryptoJS.mode.CBC,  
		 padding: CryptoJS.pad.Pkcs7
	});
	return res.ciphertext.toString();
}
function preloadimages(arr){
    var newimages=[], loadedimages=0;
    var postaction=function(){} ;
    var arr=(typeof arr!="object")? [arr] : arr;
    function imageloadpost(){
        loadedimages++;
        if (loadedimages==arr.length){
            postaction(newimages);
        }
    }
    for (var i=0; i<arr.length; i++){
        newimages[i]=new Image();
        newimages[i].src=arr[i];
        newimages[i].onload=function(){
            imageloadpost()
        };
        newimages[i].onerror=function(){
            imageloadpost()
        }
    }
    return {
        done:function(f){
            postaction=f || postaction
        }
    }
}



function setCookie(name,value,time){
    if(!name||!value) return;
    var times = time||120;//默认120秒
    var exp  = new Date();
    exp.setTime(exp.getTime() + times*1000);
    document.cookie = name + "="+ encodeURIComponent(value) +";expires="+ exp.toUTCString();
}

//获取cookie
function getCookie(name){
    var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
    if(arr != null) return decodeURIComponent(arr[2]);
    return null;
}

//删除cookie
function delCookie(name){
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(!cval) document.cookie=name +"="+cval+";expires="+exp.toUTCString();
}

//发送手机短信===========

    var SMSTIMES =120;
    var smsStatus =false
    var isSending =false
    var iscode = false
    var timeStop;

    function countTims(cacheTime){
        var cacheTime = getCookie('SMSTIMES')
        $('#smslog').html('请输入验证码')
        if(!cacheTime){
            $('#smscode').focus()
            setCookie('SMSTIMES',SMSTIMES,SMSTIMES)
            var times = getCookie('SMSTIMES')
            timeStop = setInterval(function(){
                if(times==0){
                    times = SMSTIMES;//当减到0时赋值为60
                    $('#smsbtn-text').html('获取验证码');
                    $('#smsbtn-text').removeClass('smsbtn-disable');
                    clearInterval(timeStop);//清除定时器
                    isSending = false
                }else{
                    var t = times--;
                    setCookie('SMSTIMES',t,t)
                    $('#smsbtn-text').html(times +' S');
                    $('#smsbtn-text').addClass('smsbtn-disable');
                }
            },1000)
        }else{
            $('#smscode').focus()
            var times = getCookie('SMSTIMES')
            timeStop = setInterval(function(){
                if(times==0){
                    times = SMSTIMES;//当减到0时赋值为60
                    $('#smsbtn-text').html('获取验证码');
                    $('#smsbtn-text').removeClass('smsbtn-disable');
                    clearInterval(timeStop);//清除定时器
                    isSending = false
                }else{
                    var t = times--;
                    setCookie('SMSTIMES',t,t)
                    $('#smsbtn-text').html(times +' S');
                    $('#smsbtn-text').addClass('smsbtn-disable');
                }
            },1000)
        }
    }





    function sendSMS(){
        if(isSending){
            return false
        }
        isSending =true
        var mobile = $("#regmobile").val()
        $('#smscode').focus()
        setCookie('SMSTIMES',SMSTIMES,SMSTIMES)
        var times = getCookie('SMSTIMES')
        //发送短信
        $.post("/sms_code",
            {mobile: mobile,send_type:1},
            function (data) {
                if (data.result == 1) {
                }else{
                    delCookie('SMSTIMES')
                    $('#smslog').html("<strong class='red'>+data.msg+“</strong>")
                    clearInterval(timeStop);//清除定时器
                    $('#smsbtn-text').removeClass('smsbtn-disable');
                    $('#smsbtn-text').html('获取验证码');
                    times = SMSTIMES;
                    isSending = true
                }
            }
        )
        timeStop = setInterval(function(){
            if(times==0){
                times = SMSTIMES;//当减到0时赋值为60
                $('#smsbtn-text').html('获取验证码');
                $('#smsbtn-text').removeClass('smsbtn-disable');
                clearInterval(timeStop);//清除定时器
                isSending = false
            }else{
                var t = times--;
                setCookie('SMSTIMES',t,t)
                $('#smsbtn-text').html(times +' S');
                $('#smsbtn-text').addClass('smsbtn-disable');
            }
        },1000)
    }



//
    function chekcCode(){
        var code = $('#smscode').val()
        if(!code||code.length<6){
            iscode=false
            $('#smslog').html('请输入验证码')
        }else{
            iscode =true
            $('#smslog').html('')
        }
    }

//发送手机验证码下一步
    function checkPhone(){
        $('#smscode').blur();
        if (iscode) {
            $(".loading").show();

            var params ={
                mobile:$("#regmobile").val(),
                code:$('#smscode').val()
            }
            $.post("/sms_code_verify",
                params,
                function (data) {
                    if (data.result == 1) {
                        //跳转到修改密码页面
                        window.location.href = "/findPwdNext2?username="+$("#regmobile").val();
                    }else{
                        $('#smsErrlog').html(data.msg)
                    }
                    $(".loading").hide();
                }
            )
        } else {
            //$.growlUI("提示", "有误,请检查输入的信息");
        }
    }




